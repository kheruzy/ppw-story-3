function on() {
    document.getElementById("overlay").style.display = "flex";
}


function off() {
    document.getElementById("overlay").style.display = "none";
}

function replace(title, text,source) {
    document.getElementById("overlay_title").innerHTML = title;
    document.getElementById("overlay_text").innerHTML = text;
    document.getElementById("overlay_pic").src=source;
    on()
}

function hi() {
    replace(
        "Hi!",
        "You can learn more about what I'm doing by clicking on object. Such as this! I really do hope you'll have a nice time here. Enjoy!",
        "profile_pic.jpg"
    );
}

function webdev() {
    replace (
        "Web Development",
        "Web development is the work involved in developing a web site for the Internet (World Wide Web) or an intranet (a private network). Web development can range from developing a simple single static page of plain text to complex web-based internet applications (web apps), electronic businesses, and social network services. A more comprehensive list of tasks to which web development commonly refers, may include web engineering, web design, web content development, client liaison, client-side/server-side scripting, web server and network security configuration, and e-commerce development.",
        "https://images.unsplash.com/photo-1467232004584-a241de8bcf5d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
    );
}

function uiux() {
    replace (
        "User Interface / User Experience",
        "User interface design (UI) or user interface engineering is the design of user interfaces for machines and software, such as computers, home appliances, mobile devices, and other electronic devices, with the focus on maximizing usability and the user experience. The goal of user interface design is to make the user's interaction as simple and efficient as possible, in terms of accomplishing user goals (user-centered design).",
        "https://images.unsplash.com/photo-1541591708423-9001fe827349?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1400&q=80"
    );
}

function itdev() {
    replace(
        "IT Development",
        "Software development is the process of conceiving, specifying, designing, programming, documenting, testing, and bug fixing involved in creating and maintaining applications, frameworks, or other software components. Software development is a process of writing and maintaining the source code, but in a broader sense, it includes all that is involved between the conception of the desired software through to the final manifestation of the software, sometimes in a planned and structured process.",
        "https://images.unsplash.com/photo-1561736778-92e52a7769ef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
    );
}

function ps() {
    replace(
        "Adobe Photoshop",
        "Adobe Photoshop is a raster graphics editor developed and published by Adobe Inc. for Windows and macOS. It was originally created in 1988 by Thomas and John Knoll. Since then, this software has become the industry standard not only in raster graphics editing, but in digital art as a whole.",
        "https://images.unsplash.com/photo-1457464901128-7608dc7561ea?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
    );
}

function ai() {
    replace(
        "Adobe Illustrator",
        "Adobe Illustrator is a vector graphics editor developed and marketed by Adobe Inc. Originally designed for the Apple Macintosh, development of Adobe Illustrator began in 1985. Along with Creative Cloud (Adobe's shift to monthly or annual subscription service delivered over the Internet), Illustrator CC was released. The latest version, Illustrator CC 2019, was released in October 2018 and is the 23rd generation in the product line. Adobe Illustrator was reviewed as the best vector graphics editing program in 2018 by PC Magazine.",
        "https://cdn.fileinfo.com/img/sw/ss/lg/adobe_illustrator_3.jpg"
    );
}

function xd() {
    replace (
        "Adobe XD",
        "Adobe XD is a vector-based user experience design tool for web apps and mobile apps, developed and published by Adobe Inc. It is available for macOS and Windows, although there are versions for iOS and Android to help preview the result of work directly on mobile devices. XD supports website wireframing, and creating simple interactive click-through prototypes.",
        "https://i.ytimg.com/vi/OicuzCSD84I/maxresdefault.jpg"
    );
}

function figma() {
    replace (
        "Figma",
        "Figma is a cloud-based design tool that is similar to Sketch in functionality and features, but with big differences that make Figma better for team collaboration. For those skeptical of such claims, we’ll explain how Figma simplifies the design process and is more effective than other programs at helping designers and teams work together efficiently.",
        "https://miro.medium.com/max/2560/1*dDjrqzvMNljHqSu6fLk_Tg.png"
    );
}

function html() {
    replace(
        "HTML",
        "Hypertext Markup Language (HTML) is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets (CSS) and scripting languages such as JavaScript. Web browsers receive HTML documents from a web server or from local storage and render the documents into multimedia web pages. HTML describes the structure of a web page semantically and originally included cues for the appearance of the document.",
        "https://miro.medium.com/max/12000/1*fXBo56b0tanSCNHo4O2eWw.jpeg"
    );
}

function css() {
    replace(
        "CSS",
        "Cascading Style Sheets (CSS) is a style sheet language used for describing the presentation of a document written in a markup language like HTML.[1] CSS is a cornerstone technology of the World Wide Web, alongside HTML and JavaScript. CSS is designed to enable the separation of presentation and content, including layout, colors, and fonts. This separation can improve content accessibility, provide more flexibility and control in the specification of presentation characteristics, enable multiple web pages to share formatting by specifying the relevant CSS in a separate .css file, and reduce complexity and repetition in the structural content.",
        "https://www.purelybranded.com/wp-content/uploads/2010/10/why-use-css-in-web-design.gif"
    )
}

function java() {
    replace(
        "Java",
        "Java is a general-purpose programming language that is class-based, object-oriented (although not a pure object-oriented language, as it contains primitive types), and designed to have as few implementation dependencies as possible. It is intended to let application developers write once, run anywhere (WORA), meaning that compiled Java code can run on all platforms that support Java without the need for recompilation.",
        "https://beginnersbook.com/wp-content/uploads/2013/12/java_string_to_long.jpg"
    );
}

function py() {
    replace(
        "Python",
        "Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant whitespace. Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects.",
        "https://miro.medium.com/max/12032/0*DypQzAMdE9cudggX"
    );
}

function oim() {
    replace(
        "OIM UI 2018",
        "OIM UI is one of the programs of BEM UI under the Ministry of Education of Science as a forum for students from the University of Indonesia to determine their abilities in the scientific field.",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToFqJDpQZRDryvxZVlogUY0ZS_kLNY_WP_UrmPp4VCQ25klLIz"
    );
}


function pemira() {
    replace(
        "Pemira Fasilkom UI 2018",
        "An election is a formal group decision-making process by which a population chooses an individual to hold public office. Elections have been the usual mechanism by which modern representative democracy has operated since the 17th century.",
        "https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1563962983/tbrfm5fqdyyfwcflit97.jpg"
    );
}

function bem() {
    replace (
        "BEM Fasilkom UI 2018",
        "The Student Executive Board (abbreviated as BEM) or the University Student Association is an intra-campus student organization which is an executive institution at the higher education level. In implementing its programs, BEM generally has several departments.",
        "https://asset-a.grid.id/crop/0x0:0x0/700x465/photo/haifoto/original/1443_fakultas-ilmu-komputer-universitas-indonesia-fasilkom-ui.jpg"
    );
}

function comp() {
    replace (
        "Compfest 11",
        "COMPFEST is the largest annual IT event held by students of the Faculty of Computer Science, University of Indonesia. Every year, COMPFEST aims to make an impact on society through the Academy, Competition, Seminar and Main Event. This year, COMPFEST will bring \"Igniting Technological Awareness on Facing the Hyperconnected Era\" as a big theme.",
        "http://seputarkampus.com/wp-content/uploads/2018/04/CSE2.jpg"
    );
}
